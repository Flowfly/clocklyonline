<?php
if(session_id() == "")
    session_start();
include_once ('BDD_connection.php');

function Export_Blocks($file, $comment)
{
    global $bdd;
        $query = $bdd->prepare("INSERT INTO blocks_save (`code`, `dateAdded`, `comment` , `idUser`) VALUES (:code, :dateAdded, :comment ,:id)");
        $query->bindValue(":code", $file);
        $query->bindValue(":comment", $comment);
        $query->bindValue(":dateAdded", date("Y-m-d H:i:s"));
        $query->bindValue(":id", $_SESSION['id'], PDO::PARAM_INT);
        $query->execute();
}

function Show_Import_Saves($id)
{
    global $bdd;
    $query = $bdd->prepare("SELECT idSave,dateAdded, comment FROM blocks_save, users WHERE blocks_save.idUser = :id AND blocks_save.idUser = users.idUser");
    $query->bindValue(":id", $id, PDO::PARAM_INT);
    $query->execute();
    return $query;
}

function GetSave($id)
{
    global $bdd;
    $valueReturned = "";
    $query = $bdd->prepare("SELECT code FROM blocks_save WHERE idSave=:id");
    $query->bindValue(":id", $id, PDO::PARAM_INT);
    $query->execute();
    while($result = $query->fetch(PDO::FETCH_ASSOC))
    {
        $valueReturned = $result['code'];
    }
    return $valueReturned;

}

if(isset($_POST['xml']))
    Export_Blocks(filter_input(INPUT_POST, 'xml'), filter_input(INPUT_POST, 'comment'));

