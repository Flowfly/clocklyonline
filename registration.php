<?php
session_start();
include_once('BDD_connection.php');

if(isset($_POST['signUp']))
{
    if(isset($_POST['firstName']) && isset($_POST['lastName']) && isset($_POST['mailRegister']) && isset($_POST['passwordRegister']) && isset($_POST['passwordConfirmation']))
    {
        if($_POST['passwordRegister'] == $_POST['passwordConfirmation'])
        {
            try {
                $firstName = $_POST['firstName'];
                $lastName = $_POST['lastName'];
                $mailRegister = $_POST['mailRegister'];
                $password = sha1($_POST['passwordRegister']);

                $query = $bdd->prepare("INSERT INTO USERS (firstName, lastName, password, mail) VALUES (:firstName, :lastName, :password, :mail)");
                $query->bindParam(':firstName', $firstName, PDO::PARAM_STR);
                $query->bindParam('lastName', $lastName, PDO::PARAM_STR);
                $query->bindParam('password', $password, PDO::PARAM_STR);
                $query->bindParam('mail', $mailRegister, PDO::PARAM_STR);
                $query->execute();
                header('Location:index.php');
            }
            catch(Exception $e)
            {
                $_SESSION['error'] = $e->getMessage();
                header('Location:index.php');
            }
        }
        else
        {
            $_SESSION['error'] = "Les 2 mots de passes ne correspondent pas !";
            header('Location:index.php');
        }
    }
}


?>
