<?php
session_start();
include_once('BDD_connection.php');
if(isset($_POST['login'])) {
    if (isset($_POST['mail']) && isset($_POST['password'])) {
        $mail = $_POST['mail'];
        $password = $_POST['password'];
        try {
            $query = $bdd->prepare("SELECT *, COUNT(mail) AS isset FROM USERS WHERE mail = :mail");
            $query->bindParam(":mail", $mail, PDO::PARAM_STR);
            $query->execute();
            while ($result = $query->fetch(PDO::FETCH_ASSOC)) {
                if ($result['isset'] == 1) {
                    if (sha1($password) == $result['password']) {
                        $_SESSION['id'] = $result['idUser'];
                        $_SESSION['firstName'] = $result['firstName'];
                        $_SESSION['lastName'] = $result['lastName'];
                        $_SESSION['mail'] = $result['mail'];
                        header('Location:index.php');
                    } else {
                        $_SESSION['error'] = "Le nom d'utilisateur ou le mot de passe est incorrect";
                        header('Location:index.php');
                    }

                } else {
                    $_SESSION['error'] = "Le nom d'utilisateur ou le mot de passe est incorrect";
                    header('Location:index.php');
                }
            }
        } catch (Exception $e) {
            $_SESSION['error'] = $e->getMessage();
            header('Location:index.php');
        }
    }
}
if(isset($_POST['logout']))
{
    $_SESSION = [];
    header('Location:index.php');
}
?>
