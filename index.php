<?php
session_start();
?>
<!doctype html>
<html>
<head>
    <link rel="stylesheet" href="css/uikit.min.css" />
    <?php if (isset($_SESSION['error'])) { ?>
        <p style="color:red;">
            <?php echo $_SESSION['error'];
            unset($_SESSION['error']); ?>
        </p>
    <?php } ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.39/css/uikit.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.39/js/uikit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.39/js/uikit-icons.min.js"></script>
</head>
<body style="position: absolute; min-height: 85%; min-width: 100%; overflow: hidden;">

<form method="post" action="login.php" id="signinform" class="uk-form">


    <?php if (!isset($_SESSION['mail'])) { ?>
        <nav class="uk-navbar-left">
            <div class="uk-navbar-left">
                <ul class="uk-navbar-nav">
                        <li><input type="email" name="mail" placeholder="Email" required class="uk-form-small"></li>
                        <li><input type="password" name="password" placeholder="Password" required class="uk-form-small"></li>
                        <li><input type="submit" name="login" value="Se connecter" class="uk-button uk-button-primary"></li>
                        <li><input type="button" onclick="showRegistrationPage()" value="S'enregistrer" class="uk-button uk-button-primary"></li>
                </ul>
            </div>
        </nav>
    <?php } else { ?>

    <p><?php echo "Bienvenue, " . $_SESSION['firstName'] . " " . $_SESSION['lastName']; ?> </p>
    <nav class="uk-navbar-left">
        <div class="uk-navbar-left">
            <ul class="uk-navbar-nav">
                <li><input type="submit" name="logout" value="Se déconnecter" class="uk-button uk-button-primary">
                </li>
                <?php } ?>
</form>
<?php if (isset($_SESSION['mail'])) {
    ?>
    <li>
        <button name="Export" id="Export" class="uk-button uk-button-primary
"> Exporter</button>
    </li>
    <li>
        <button class="uk-button uk-button-primary">
        <a href="user_import.php?id=<?php echo $_SESSION['id']; ?>" style="color:white;">Importer</a>
        </button>
    </li>
    </ul>
    </nav>
<?php } ?>
<nav class="uk-navbar-container" uk-navbar>
    <div class="uk-navbar-left">
        <ul class="uk-navbar-nav">
            <li>
                <button name="ExportToFile" id="ExportToFile" class="uk-button uk-button-primary">Export to file
                </button>
            </li>
            <li>
                <input type="file" id="myFile" name="myFile" class="uk-form-file"/>
            </li>
            <li>
                <button name="ImportFromFile" id="ImportFromFile" class="uk-button uk-button-primary">Import from file
                </button>
            </li>
        </ul>
    </div>
</nav>
<div id="boxArea">
    <xml id="toolbox" style="display: none">
        <category name="Logic" colour="210">

            <block type="controls_if"></block>
            <block type="controls_if">
                <mutation else="1"></mutation>
            </block>
            <block type="controls_if">
                <mutation elseif="1" else="1"></mutation>
            </block>
            <block type="logic_compare"></block>
            <block type="logic_operation"></block>
            <block type="logic_negate"></block>
            <block type="logic_boolean"></block>
            <block type="logic_null"></block>
            <block type="logic_ternary"></block>
        </category>
        <category name="Loops" colour="120">
            <block type="controls_repeat_ext">
                <value name="TIMES">
                    <block type="math_number">
                        <field name="NUM">10</field>
                    </block>
                </value>
            </block>
            <block type="controls_whileUntil"></block>
            <block type="controls_for">
                <field name="VAR">i</field>
                <value name="FROM">
                    <block type="math_number">
                        <field name="NUM">1</field>
                    </block>
                </value>
                <value name="TO">
                    <block type="math_number">
                        <field name="NUM">10</field>
                    </block>
                </value>
                <value name="BY">
                    <block type="math_number">
                        <field name="NUM">1</field>
                    </block>
                </value>
            </block>
            <block type="controls_forEach"></block>
            <block type="controls_flow_statements"></block>
        </category>
        <category name="Math" colour="230">
            <block type="math_number"></block>
            <block type="math_arithmetic"></block>
            <block type="math_single"></block>
            <block type="math_trig"></block>
            <block type="math_constant"></block>
            <block type="math_number_property"></block>
            <block type="math_round"></block>
            <block type="math_on_list"></block>
            <block type="math_modulo"></block>
            <block type="math_constrain">
                <value name="LOW">
                    <block type="math_number">
                        <field name="NUM">1</field>
                    </block>
                </value>
                <value name="HIGH">
                    <block type="math_number">
                        <field name="NUM">100</field>
                    </block>
                </value>
            </block>
            <block type="math_random_int">
                <value name="FROM">
                    <block type="math_number">
                        <field name="NUM">1</field>
                    </block>
                </value>
                <value name="TO">
                    <block type="math_number">
                        <field name="NUM">100</field>
                    </block>
                </value>
            </block>
            <block type="math_random_float"></block>
        </category>
        <category name="Lists" colour="#82589B">
            <block type="lists_create_empty"></block>
            <block type="lists_create_with"></block>
            <block type="lists_repeat">
                <value name="NUM">
                    <block type="math_number">
                        <field name="NUM">5</field>
                    </block>
                </value>
            </block>
            <block type="lists_length"></block>
            <block type="lists_isEmpty"></block>
            <block type="lists_indexOf"></block>
            <block type="lists_getIndex"></block>
            <block type="lists_setIndex"></block>
        </category>
        <category name="Variables" custom="VARIABLE" colour="330"></category>
        <category name="Functions" custom="PROCEDURE" colour="#A2579B"></category>
    </xml>

    <div class="uk-grid">
      <div id="bloclyContainerDiv" class="uk-width-2-3" >
         <div id="blocklyArea" style="position: absolute; min-height: 90%;" class="uk-width-2-3"></div>
         <div id="blocklyDiv"style="position: absolute;"></div>
      </div>
      <div class="uk-width-1-3">
        <div uk-grid class="uk-margin-remove-left uk-margin-small-right">
          <button class="uk-button uk-button-default uk-button-small uk-width-1-2" type="button">Selection du language</button>
            <div uk-dropdown>
                <li>
                    <a id="jsLanguage" onclick="ExportSourceCode('js')">Javascript</a>
                </li>
                <li>                    
                <a id="pythonLanguage" onclick="ExportSourceCode('py')">Python</a>
                </li>
            <li><a id="csharpLanguage">C#</a></li>
                <li>
                    <a id="phpLanguage" onclick="ExportSourceCode('php')">PHP</a>
                </li>
                <li>
                    <a id="luaLanguage" onclick="ExportSourceCode('lua')">Lua</a>
                </li>
                <li>
                    <a id="dartLanguage" onclick="ExportSourceCode('dart')">Dart</a>
                </li>
            </div>
          <button class="uk-button uk-button-default uk-button-small uk-width-1-2" id="ExportationCode">Export</button>
          <textarea readonly id="codeText" class="uk-form-help-block uk-margin-remove" style="width: 100%;height:750px;" ></textarea>
      </div>
    </div>

<div id="registrationArea" style="display:none;">
    <form method="post" action="registration.php">
        <table>
            <tr>
                <td>Prénom :</td>
                <td><input type="text" name="firstName" required></td>
            </tr>
            <tr>
                <td>Nom :</td>
                <td><input type="text" name="lastName" required></td>
            </tr>
            <tr>
                <td>Email :</td>
                <td><input type="email" name="mailRegister" required></td>
            </tr>
            <tr>
                <td>Mot de passe :</td>
                <td><input type="password" name="passwordRegister" required></td>
            </tr>
            <tr>
                <td>Confirmer mot de passe :</td>
                <td><input type="password" name="passwordConfirmation" required></td>
            </tr>
            <tr>
                <td></td>
                <td style="text-align: right;"><input type="submit" name="signUp"></td>
            </tr>
        </table>
    </form>

</div>
</body>

<script>

    function showRegistrationPage() {
        if (boxArea.style.display == "none") {
            boxArea.style.display = "inline-block";
            registrationArea.style.display = "none";
            signinform.style.display = "inline-block";
        }
        else {
            boxArea.style.display = "none";
            registrationArea.style.display = "inline-block";
            signinform.style.display = "none";
        }
    }

    $("#ExportationCode").click(function (e) {
        e.preventDefault;
        /*function ExportSourceCode(code) {
*/
        var contenu = document.getElementById("codeText").value;
        var expr = code;

        let defaultFilename = "Source_Code";
        var blob = new Blob([contenu], { type: "text/plain;charset=utf-8" });
        saveAs(blob, defaultFilename + "." + code);

        var fileInput = document.querySelector('#myFile');
        fileInput.addEventListener('change', function () {

            var reader = new FileReader();

            reader.addEventListener('load', function () {
                texte = reader.result;
            });

            reader.readAsText(fileInput.files[0]);
        });
        //}
    });

    var code = "";
    function ExportSourceCode(language)
    {
        code = language;
    }

    //retrieves the code generated by the blocks after the click
    $("#Export").click(function (e) {
        e.preventDefault;
        var commentBlocks = prompt("Saisir un commentaire");
        if (commentBlocks != null) {
            console.log(commentBlocks);
        }
        var xml = Blockly.Xml.workspaceToDom(workspacePlayground);
        var xml_text = Blockly.Xml.domToText(xml);

        $.ajax({
            type: "POST",
            url: 'blocks_methods.php',
            dataType: 'json',
            data: {
                xml: xml_text,
                comment: commentBlocks
            },
            success: function (data) {
                console.log(data);
            }
        });

    });


    $("#Import").click(function (e) {
        e.preventDefault;
        var xml = Blockly.Xml.textToDom(texte);
        Blockly.Xml.domToWorkspace(xml, workspacePlayground);
    });

    $("#ExportToFile").click(function (e) {
        e.preventDefault;

        var xml = Blockly.Xml.workspaceToDom(workspacePlayground);
        xml_text = Blockly.Xml.domToPrettyText(xml);

        //var BlockExport = $(".blocklyBlockCanvas").html();
        let defaultFilename = "BlocksExport";
        var blob = new Blob([xml_text], {type: "text/plain;charset=utf-8"});
        saveAs(blob, defaultFilename + ".xml");
    });

    var fileInput = document.querySelector('#myFile');
    fileInput.addEventListener('change', function () {

        var reader = new FileReader();

        reader.addEventListener('load', function () {
            texte = reader.result;
        });

        reader.readAsText(fileInput.files[0]);

    });

    $("#ImportFromFile").click(function (e) {
        e.preventDefault;
        var xml = Blockly.Xml.textToDom(texte);
        Blockly.Xml.domToWorkspace(xml, workspacePlayground);
    });


</script>

<script src="js/uikit.min.js"></script>
<script src="js/uikit-icons.min.js"></script>
<script src="blockly_compressed.js"></script>
<script src="blocks_compressed.js"></script>
<script src="javascript_compressed.js"></script>
<script src="python_compressed.js"></script>
<script src="php_compressed.js"></script>
<script src="lua_compressed.js"></script>
<script src="dart_compressed.js"></script>
<script src="csharp_compressed.js"></script>
<script src="msg/js/en.js"></script>
<script src="init.js"></script>


<!--<script src="FileSaver/FileSaver.js" type="text/javascript"></script>-->

<script src="FileSaver/FileSaver.js" type="text/javascript"></script>
<?php
if (isset($_POST['idSave'])) {
    include_once('blocks_methods.php');
    $code = GetSave($_POST['idSave']);
    ?>

    <script>
        var xml = Blockly.Xml.textToDom(<?php echo "\"" . str_replace("\"", "\\\"", $code) . "\"";  ?>);
        console.log(xml);
        Blockly.Xml.domToWorkspace(xml, workspacePlayground);
    </script>
    <?php
}
?>
</html>
