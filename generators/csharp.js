// Do not edit
'use strict';
goog.provide('Blockly.csharp');

goog.require('Blockly.Generator');

Blockly.csharp = new Blockly.Generator('csharp');

Blockly.csharp.addReservedWords(
	"abstract, as ,base, bool, break, byte, case, catch, char, checked, class, const, continue, decimal, default, delegate," +
	"do, double, else, enum, event, explicit, extern, false, finally, fixed, float, for, foreach, goto, if, implicit, in, int," +
	"interface, internal, is, lock, long, namespace, new, null, object, operator, out, override, params, private, protected," +
	"public, readonly, ref, return, sbyte, sealed, short, sizeof, stackalloc, static, string, struct, switch, this, throw," +
	"true, try, typeof, uint, ulong, unchecked, unsafe, ushort, using, virtual, void, volatile, while, add, alias, ascending," +
	"async, await, descending, dynamic, from, get, global, group, into, join, let, orderby, partial, remove, select, set," +
	"value, var, where, yield"
);

Blockly.csharp.ORDER_ATOMIC = 0;         // 0 ""
Blockly.csharp.ORDER_MEMBER = 1;         // . []
Blockly.csharp.ORDER_NEW = 1;            // new
Blockly.csharp.ORDER_TYPEOF = 1;         // typeof
Blockly.csharp.ORDER_FUNCTION_CALL = 1;  // ()
Blockly.csharp.ORDER_INCREMENT = 1;      // ++
Blockly.csharp.ORDER_DECREMENT = 1;      // --
Blockly.csharp.ORDER_LOGICAL_NOT = 2;    // !
Blockly.csharp.ORDER_BITWISE_NOT = 2;    // ~
Blockly.csharp.ORDER_UNARY_PLUS = 2;     // +
Blockly.csharp.ORDER_UNARY_NEGATION = 2; // -
Blockly.csharp.ORDER_MULTIPLICATION = 3; // *
Blockly.csharp.ORDER_DIVISION = 3;       // /
Blockly.csharp.ORDER_MODULUS = 3;        // %
Blockly.csharp.ORDER_ADDITION = 4;       // +
Blockly.csharp.ORDER_SUBTRACTION = 4;    // -
Blockly.csharp.ORDER_BITWISE_SHIFT = 5;  // << >>
Blockly.csharp.ORDER_RELATIONAL = 6;     // < <= > >=
Blockly.csharp.ORDER_EQUALITY = 7;       // == !=
Blockly.csharp.ORDER_BITWISE_AND = 8;   // &
Blockly.csharp.ORDER_BITWISE_XOR = 9;   // ^
Blockly.csharp.ORDER_BITWISE_OR = 10;    // |
Blockly.csharp.ORDER_LOGICAL_AND = 11;   // &&
Blockly.csharp.ORDER_LOGICAL_OR = 12;    // ||
Blockly.csharp.ORDER_CONDITIONAL = 13;   // ?:
Blockly.csharp.ORDER_ASSIGNMENT = 14;    // = += -= *= /= %= <<= >>= ...
Blockly.csharp.ORDER_COMMA = 15;         // ,
Blockly.csharp.ORDER_NONE = 99;


Blockly.csharp.INFINITE_LOOP_TRAP = null;


Blockly.csharp.init = function(a){
    Blockly.csharp.definitions_ = {};

    if (Blockly.Variables) {
        if (!Blockly.csharp.variableDB_) {
            Blockly.csharp.variableDB_ = new Blockly.Names(Blockly.csharp.RESERVED_WORDS_);
        } else {
            Blockly.csharp.variableDB_.reset();
        }

        var defvars = [];
        var variables = Blockly.Variables.allVariables(a);
        for (var x = 0; x < variables.length; x++) {
            defvars[x] = 'dynamic' + Blockly.csharp.variableDB_.getName(variables[x], Blockly.Variables.NAME_TYPE) + ';';
        }
        Blockly.csharp.definitions_['variables'] = defvars.join('\n');
    }
};

/* Prepend the generated code with the variable definitions. */
Blockly.csharp.finish = function(code){
    var definitions = [];
    for (var name in Blockly.csharp.definitions_) {
        definitions.push(Blockly.csharp.definitions_[name]);
    }
    return definitions.join('\n\n') + '\n\n\n' + code;
};

/**
 * Naked values are top-level blocks with outputs that aren't plugged into
 * anything.  A trailing semicolon is needed to make this legal.
 * @param {string} line Line of generated code.
 * @return {string} Legal line of code.
 */
Blockly.csharp.scrubNakedValue = function(line) {
    return line + ';\n';
};

Blockly.csharp.quote_ = function(val) {
    return goog.string.quote(val);
};


Blockly.csharp.scrub_ = function(block, code) {
    if (code === null) {
        return '';
    }

    var commentCode = '';

    if (!block.outpurConnection || !block.outputConnection.targetConnection) {
        var comment = block.getCommentText();

        if (comment) {
            commentCode += this.prefixLines(comment, '// ') + '\n';
        }

        for (var x = 0; x < block.inputList.length; x++)
        {
            if(block.inputList[x].type == Blockly.INPUT_VALUE) {
                var childBlock = block.inputList[x].connection.targetBlock();

                if(childBlock) {
                    var comment = this.allNestedComments(childBlock);

                    if(comment) {
                        commentCode += this.prefixLines(comment, '// ');
                    }
                }
            }
        }
    }

    var nextBlock = block.nextConnection && block.netConnection.targetBlock();
    var nextCode = this.blockToCode(nextBlock);

    return commentCode + code + nextCode;
};
