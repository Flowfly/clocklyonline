'use strict';

Blockly.csharp.variables = {};

Blockly.csharp.variables_get = function() {
  // Variable getter.
  var code = Blockly.csharp.variableDB_.getName(this.getTitleValue('VAR'),
      Blockly.Variables.NAME_TYPE);
  return [code, Blockly.csharp.ORDER_ATOMIC];
};

Blockly.csharp.variables_set = function() {
  // Variable setter.
  var argument0 = Blockly.csharp.valueToCode(this, 'VALUE',
      Blockly.csharp.ORDER_ASSIGNMENT) || 'null';
  var varName = Blockly.csharp.variableDB_.getName(
      this.getTitleValue('VAR'), Blockly.Variables.NAME_TYPE);
  return varName + ' = ' + argument0 + ';\n';
};