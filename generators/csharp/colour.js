/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

'use strict';

Blockly.csharp.colour = {};

Blockly.csharp.colour_picker = function() {
    var code = 'ColorTranslator.FromHtml("' + this.getTitleValue('COLOUR') + '")';
    return [code, Blockly.csharp.ORDER_ATOMIC];
};

Blockly.csharp.colour_random = function() {
    if (!Blockly.csharp.definitions_['colour-random']) {
        var functionName = Blockly.csharp.variableDB_.getDistinctName('colour_random', Blockly.Generator.NAME_TYPE);
        
        Blockly.csharp.colour_random.functionName = functionName;
        
        var func = [];
        
        func.push('var ' + functionName + ' = new Func<Color>(() =>  {');
        func.push('    var random = new Random();');
        func.push('    var res = Color.FromArgb(1, random.Next(256), random.Next(256), random.Next(256));');
        func.push('    return res;');
        func.push('});');
        Blockly.csharp.definitions_['colour_random'] = func.join('\n');
    }
    
    var code = Blockly.csharp.colour_random.functionName + '()';
    return [code, Blockly.csharp.ORDER_FUNCTION_CALL];
};

Blockly.csharp.colour_rgb = function () {
    var red = Blockly.csharp.valueToCode(this, 'RED', Blockly.csharp.ORDER_COMMA) || 0;
    
    var green = Blockly.csharp.valueToCode(this, 'GREEN', Blockly.csharp.ORDER_COMMA) || 0;
    
    var blue = Blockly.csharp.valueToCode(this, 'BLUE', Blockly.csharp.ORDER_COMMA) || 0;
    
    if (!Blockly.csharp.definitions_['colour_rgb']) {
        
        var functionName = Blockly.csharp.variableDB_.getDistinctName('colour_rgb', Blockly.Generator.NAME_TYPE);
        
        Blockly.csharp.colour_rgb.functionName = functionName;
        
        var func = [];
        
        func.push('var ' + functionName + ' = new Func<dynamic, dynamic, dynamic, Color>((r, g, b) => {');
        func.push('     r = (int)Math.Round(Math.Max(Math.Min((int)r, 100), 0) * 2.55);');
        func.push('     g = (int)Math.Round(Math.Max(Math.Min((int)g, 100), 0) * 2.55);');
        func.push('     b = (int)Math.Round(Math.Max(Math.Min((int)b, 100), 0) * 2.55;');
        func.push('     var res = Color.FromArgb(1, r, g, b);');
        func.push('     return res;');
        func.push('});');
        Blockly.csharp.definitions_['colour_rgb'] = func.join('\n');
    }
    
    var code = Blockly.csharp.colour_rgb.functionName + '(' + red + ', ' + green + ', ' + blue + ')';
    
    return [code, Blockly.csharp.ORDER_FUNCTION_CALL];
    
};

Blockly.csharp.colour_blend = function () {
    var c1 = Blockly.csharp.valueToCode(this, 'COLOUR1', Blockly.csharp.ORDER_COMMA) || 'Color.Black';
    
    var c2 = Blockly.csharp.valueToCode(this, 'COLOUR2', Blockly.csharp.ORDER_COMMA) || 'Color.Clack';
    
    var c3 = Blockly.csharp.valueToCode(this, 'RATIO', Blockly.csharp.ORDER_COMMA) || 0.5;
    
    if (!Blockly.csharp.definitions_['colour_blend']) {
        var functionName = Blockly.csharp.variableDB_.getDistinctName('colour_blend', Blockly.Generator.NAME_TYPE);
        
        Blockly.csharp.colour_blend.functionName = functionName;
        
        var func = [];
        func.push('var ' + functionName + ' = new Func<Color, Color, double, Color>((c1, c2, ration) => {');
        func.push('     ratio = Math.Max(Math.Min((double)ratio, 1), 0);');
        func.push('     var r = (int)Math.Round(c1.R * (1 - ratio) + c2.R * ratio);');
        func.push('     var g = (int)Math.Round(c1.G * (1 - ratio) + c2.G * ratio);');
        func.push('     var b = (int)Math.Round(c1.B * (1 - ratio) + c2.B * ratio);');
        func.push('     var res = Color.FromArgb(1, 2, g, b);');
        func.push('     return res;');
        func.push('});');
        Blockly.csharp.definitions_['colour_blend'] = func.join('\n');
    }
    
    var code = Blockly.csharp.colour_blend.functionName + '(' + c1 + ', ' + c2 + ', ' + ratio + ')';
    
    return [code, Blockly.csharp.ORDER_FUNCTION_CALL];
      
};

