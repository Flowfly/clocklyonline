'use strict';

Blockly.csharp.logic = {};

Blockly.csharp.controls_if = function() {
  // If/elseif/else condition.
  var n = 0;
  var argument = Blockly.csharp.valueToCode(this, 'IF' + n,
      Blockly.csharp.ORDER_NONE) || 'false';
  var branch = Blockly.csharp.statementToCode(this, 'DO' + n);
  var code = 'if (' + argument + ') {\n' + branch + '}';
  for (n = 1; n <= this.elseifCount_; n++) {
    argument = Blockly.csharp.valueToCode(this, 'IF' + n,
        Blockly.csharp.ORDER_NONE) || 'false';
    branch = Blockly.csharp.statementToCode(this, 'DO' + n);
    code += ' else if (' + argument + ') {\n' + branch + '}\n';
  }
  if (this.elseCount_) {
    branch = Blockly.csharp.statementToCode(this, 'ELSE');
    code += ' else {\n' + branch + '}\n';
  }
  return code + '\n';
};

Blockly.csharp.logic_compare = function() {
  // Comparison operator.
  var mode = this.getTitleValue('OP');
  var operator = Blockly.csharp.logic_compare.OPERATORS[mode];
  var order = (operator == '==' || operator == '!=') ?
      Blockly.csharp.ORDER_EQUALITY : Blockly.csharp.ORDER_RELATIONAL;
  var argument0 = Blockly.csharp.valueToCode(this, 'A', order) || 'null';
  var argument1 = Blockly.csharp.valueToCode(this, 'B', order) || 'null';
  var code = argument0 + ' ' + operator + ' ' + argument1;
  return [code, order];
};

Blockly.csharp.logic_compare.OPERATORS = {
  EQ: '==',
  NEQ: '!=',
  LT: '<',
  LTE: '<=',
  GT: '>',
  GTE: '>='
};

Blockly.csharp.logic_operation = function() {
  // Operations 'and', 'or'.
  var operator = (this.getTitleValue('OP') == 'AND') ? '&&' : '||';
  var order = (operator == '&&') ? Blockly.csharp.ORDER_LOGICAL_AND :
      Blockly.csharp.ORDER_LOGICAL_OR;
  var argument0 = Blockly.csharp.valueToCode(this, 'A', order) || 'false';
  var argument1 = Blockly.csharp.valueToCode(this, 'B', order) || 'false';
  var code = argument0 + ' ' + operator + ' ' + argument1;
  return [code, order];
};

Blockly.csharp.logic_negate = function() {
  // Negation.
  var order = Blockly.csharp.ORDER_LOGICAL_NOT;
  var argument0 = Blockly.csharp.valueToCode(this, 'BOOL', order) ||
      'false';
  var code = '!' + argument0;
  return [code, order];
};

Blockly.csharp.logic_boolean = function() {
  // Boolean values true and false.
  var code = (this.getTitleValue('BOOL') == 'TRUE') ? 'true' : 'false';
  return [code, Blockly.csharp.ORDER_ATOMIC];
};

Blockly.csharp.logic_null = function() {
  // Null data type.
  return ['null', Blockly.csharp.ORDER_ATOMIC];
};

Blockly.csharp.logic_ternary = function() {
  // Ternary operator.
  var value_if = Blockly.csharp.valueToCode(this, 'IF',
      Blockly.csharp.ORDER_CONDITIONAL) || 'false';
  var value_then = Blockly.csharp.valueToCode(this, 'THEN',
      Blockly.csharp.ORDER_CONDITIONAL) || 'null';
  var value_else = Blockly.csharp.valueToCode(this, 'ELSE',
      Blockly.csharp.ORDER_CONDITIONAL) || 'null';
  var code = value_if + ' ? ' + value_then + ' : ' + value_else
  return [code, Blockly.csharp.ORDER_CONDITIONAL];
};