var blocklyArea = document.getElementById('blocklyArea');
var blocklyDiv = document.getElementById('blocklyDiv');
var workspacePlayground = Blockly.inject(blocklyDiv,{toolbox: document.getElementById('toolbox')});

var onresize = function(e) {
    // Compute the absolute coordinates and dimensions of blocklyArea.
    var element = blocklyArea;
    var x = 0;
    var y = 0;
    do {
        x += element.offsetLeft;
        y += element.offsetTop;
        element = element.offsetParent;
    } while (element);
    // Position blocklyDiv over blocklyArea.
    blocklyDiv.style.left = x + 'px';
    blocklyDiv.style.top = y + 'px';
    blocklyDiv.style.width = blocklyArea.offsetWidth + 'px';
    blocklyDiv.style.height = blocklyArea.offsetHeight + 'px';
};
window.addEventListener('resize', onresize, false);
onresize();
Blockly.svgResize(workspacePlayground);

workspacePlayground.addChangeListener(myUpdateFunction);

function myUpdateFunction(event,chosenLanguage) {
  var urlParams = new URLSearchParams(window.location.search);

var code;
switch (chosenLanguage) {
  case "javascript":
    code = Blockly.JavaScript.workspaceToCode(workspacePlayground);
    break;
  case "python":
    code = Blockly.Python.workspaceToCode(workspacePlayground);
    break;
  case "php":
    code = Blockly.PHP.workspaceToCode(workspacePlayground);
    break;
  case "lua":
    code = Blockly.Lua.workspaceToCode(workspacePlayground);
    break;
  case "dart":
    code = Blockly.Dart.workspaceToCode(workspacePlayground);
    break;
  case "csharp":
    code = Blockly.csharp.workspaceToCode(workspacePlayground);
    break;
  default:
    code = Blockly.JavaScript.workspaceToCode(workspacePlayground);
    break;
}
document.getElementById('codeText').value = code;
}

$("#jsLanguage").click(function (e) {
  myUpdateFunction(e,"javascript")
});
$("#pythonLanguage").click(function (e) {
  myUpdateFunction(e,"python")
});
$("#phpLanguage").click(function (e) {
  myUpdateFunction(e,"php")
});
$("#luaLanguage").click(function (e) {
  myUpdateFunction(e,"lua")
});
$("#dartLanguage").click(function (e) {
  myUpdateFunction(e,"dart")
});
$("#csharpLanguage").click(function (e) {
  myUpdateFunction(e,"csharp")
});
