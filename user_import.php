<?php
include_once ('blocks_methods.php');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Sauvegardes de <?php echo $_SESSION['firstName'] . " " . $_SESSION['lastName']; ?></title>
</head>
<body>
    <table>
        <tr>
            <th>Sauvegardes</th>
            <th>Dates</th>
            <th>Commentaires</th>
            <th>Actions</th>

        </tr>
        <?php
        $datas = Show_Import_Saves($_GET['id']);
        $count = 0;
        while($result = $datas->fetch(PDO::FETCH_ASSOC))
        {
            $count++;
            ?>
            <tr>
                <form method="post" action="index.php">
                <td><?php echo "Sauvegarde n°" . $count; ?></td>
                <td><?php echo "Enregistré le " . $result['dateAdded']; ?></td>
                <td><?php echo $result['comment']; ?></td>
                <input type="hidden" name="idSave" value="<?php echo $result['idSave']; ?>">
                    <td><input type="submit"  value="Utiliser"></td>
                </form>
            </tr>
            <?php
        }
        ?>
    </table>
</body>
</html>